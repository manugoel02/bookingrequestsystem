package com.practice.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.practice.model.BookingRequest;
import com.practice.model.ScheduledMeetings;
import junit.framework.Assert;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author manugoel
 */
public class OutputFormatterTest {

    @Test
    public void testOutputString(){
        Map<LocalDate,Set<BookingRequest>> requests = new HashMap<LocalDate, Set<BookingRequest>>();
        LocalDate date1 = new LocalDate(2015,8,25);
        Set<BookingRequest> requestSet1 = new TreeSet<BookingRequest>();
        BookingRequest request1 = new BookingRequest("EM002",new LocalTime(9,0),new LocalTime(11,0));
        BookingRequest request2 = new BookingRequest("EM004",new LocalTime(12,0),new LocalTime(13,0));
        requestSet1.add(request1);
        requestSet1.add(request2);
        requests.put(date1, requestSet1);
        date1 = new LocalDate(2015,8,23);
        requestSet1 = new TreeSet<BookingRequest>();
        BookingRequest request3 = new BookingRequest("EM003",new LocalTime(9,0),new LocalTime(11,0));
        requestSet1.add(request3);
        requests.put(date1, requestSet1);
        ScheduledMeetings meetings = new ScheduledMeetings(new LocalTime(9,0),new LocalTime(17,30), requests);
        String expectedOutput ="2015-08-23\n09:00 11:00 EM003\n2015-08-25\n09:00 11:00 EM002\n12:00 13:00 EM004\n";
        Assert.assertEquals(expectedOutput,OutputFormatter.outputString(meetings).toString());
    }
}
