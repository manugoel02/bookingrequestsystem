package com.practice.util;

import java.util.Set;

import com.practice.model.BookingRequest;
import com.practice.model.ScheduledMeetings;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.*;

/**
 *
 * @author manugoel
 */
public class MeetingSchedulerTest {
    MeetingScheduler scheduler;

    @Before
    public void setup(){
        scheduler = MeetingScheduler.getInstance();
    }

    @Test
    public void testInvalidMeeting(){
        LocalTime officeStartTime = new LocalTime(9,0);
        LocalTime officeEndTime = new LocalTime(17,30);
        LocalTime meetingStartTime = new LocalTime(9,0);
        LocalTime meetingEndTime = new LocalTime(11,30);
        Assert.assertEquals(false,scheduler.invalidMeeting(officeStartTime, officeEndTime, meetingStartTime, meetingEndTime));
    }

    @Test
    public void testParseRecord(){
        String record = "2015-08-17 10:17:06 EMP001";
        LocalTime officeStartTime = new LocalTime(9,0);
        LocalTime officeEndTime = new LocalTime(17,30);
        String meetingStots[] = "2015-08-21 09:00 2".split(" ");
        BookingRequest requestObject = scheduler.parseRecord(record, officeStartTime, officeEndTime, meetingStots);
        Assert.assertNotNull(requestObject);
        Assert.assertEquals("EMP001",requestObject.getEmpId());
        Assert.assertEquals(9, requestObject.getMeetingStartTime().getHourOfDay());
        Assert.assertEquals(0, requestObject.getMeetingStartTime().getMinuteOfHour());
        Assert.assertEquals(11, requestObject.getMeetingEndTime().getHourOfDay());
        Assert.assertEquals(0, requestObject.getMeetingEndTime().getMinuteOfHour());
    }


    @Test
    public void testSplitData(){
        String requestData = "2015-08-17 10:17:06 EMP001";
        String regex = "\\s";
        String outputData[] = scheduler.splitData(requestData, regex);
        Assert.assertEquals(3, outputData.length);
        Assert.assertEquals("2015-08-17",outputData[0]);
        Assert.assertEquals("10:17:06",outputData[1]);
        Assert.assertEquals("EMP001",outputData[2]);
    }

    @Test
    public void testReceiveRequest(){
        String requestData = "0900 1730"+"\n"+"2015-08-17 10:17:06 EMP001"+"\n"
                +"2015-08-16 11:00 2"+"\n"+"2015-08-18 12:34:56 EMP002"+"\n"+"2015-08-21 09:00 2";
        ScheduledMeetings meetings = scheduler.receiveRequest(requestData);
        Assert.assertEquals(2,meetings.getBookingRequest().size());
        Assert.assertEquals(9, meetings.getOfficeStartTime().getHourOfDay());
        Assert.assertEquals(0, meetings.getOfficeStartTime().getMinuteOfHour());
        Assert.assertEquals(17, meetings.getOfficeEndTime().getHourOfDay());
        Assert.assertEquals(30, meetings.getOfficeEndTime().getMinuteOfHour());
        Assert.assertEquals(true,meetings.getBookingRequest().keySet().contains(new LocalDate(2015,8,16)));
        Assert.assertEquals(true,meetings.getBookingRequest().keySet().contains(new LocalDate(2015,8,21)));
        Assert.assertEquals(1,meetings.getBookingRequest().get(new LocalDate(2015,8,16)).size());
        Assert.assertEquals(1,meetings.getBookingRequest().get(new LocalDate(2015,8,21)).size());
    }

    @Test
    public void testOutSideOfficeHoursMeeting(){
        LocalTime officeStartTime = new LocalTime(9,0);
        LocalTime officeEndTime = new LocalTime(17,30);
        LocalTime meetingStartTime = new LocalTime(7,0);
        LocalTime meetingEndTime = new LocalTime(11,30);
        Assert.assertEquals(true,scheduler.invalidMeeting(officeStartTime, officeEndTime, meetingStartTime, meetingEndTime));
    }


    @Test
    public void testOverlapMeetingRequest(){
        String requestData = "0900 1730"+"\n"+"2015-08-17 10:17:06 EMP001"+"\n"
                +"2015-08-16 09:00 2"+"\n"+"2015-08-18 12:34:56 EMP002"+"\n"+"2015-08-16 09:00 2";
        ScheduledMeetings meetings = scheduler.receiveRequest(requestData);
        Assert.assertEquals(1,meetings.getBookingRequest().size());
        Assert.assertEquals(9, meetings.getOfficeStartTime().getHourOfDay());
        Assert.assertEquals(0, meetings.getOfficeStartTime().getMinuteOfHour());
        Assert.assertEquals(17, meetings.getOfficeEndTime().getHourOfDay());
        Assert.assertEquals(30, meetings.getOfficeEndTime().getMinuteOfHour());
        Assert.assertEquals(true,meetings.getBookingRequest().keySet().contains(new LocalDate(2015,8,16)));
        Assert.assertEquals(1,meetings.getBookingRequest().get(new LocalDate(2015,8,16)).size());
    }

    @Test
    public void testParsingOfficeHours(){
        String requestData = "0900 1730"+"\n"+"2015-08-17 10:17:06 EMP001"+"\n"
                +"2015-08-16 09:00 2";
        ScheduledMeetings meetings = scheduler.receiveRequest(requestData);
        Assert.assertEquals(9, meetings.getOfficeStartTime().getHourOfDay());
        Assert.assertEquals(0, meetings.getOfficeStartTime().getMinuteOfHour());
        Assert.assertEquals(17, meetings.getOfficeEndTime().getHourOfDay());
        Assert.assertEquals(30, meetings.getOfficeEndTime().getMinuteOfHour());
    }


    @Test
    public void testParsMeetingRequest(){
        String requestData = "0900 1730"+"\n"+"2015-08-17 10:17:06 EMP001"+"\n"
                +"2015-08-16 09:00 2";
        ScheduledMeetings meetings = scheduler.receiveRequest(requestData);
        Set<BookingRequest> requests = meetings.getBookingRequest().get(new LocalDate(2015,8,16));
        Object[] meetingsRequests =  requests.toArray();
        Assert.assertEquals(9, meetings.getOfficeStartTime().getHourOfDay());
        Assert.assertEquals(0, meetings.getOfficeStartTime().getMinuteOfHour());
        Assert.assertEquals(17, meetings.getOfficeEndTime().getHourOfDay());
        Assert.assertEquals(30, meetings.getOfficeEndTime().getMinuteOfHour());
        Assert.assertEquals(1, meetingsRequests.length);
        Assert.assertEquals("EMP001", ((BookingRequest)meetingsRequests[0]).getEmpId());
        Assert.assertEquals(9, ((BookingRequest)meetingsRequests[0]).getMeetingStartTime().getHourOfDay());
        Assert.assertEquals(0, ((BookingRequest)meetingsRequests[0]).getMeetingStartTime().getMinuteOfHour());
        Assert.assertEquals(11, ((BookingRequest)meetingsRequests[0]).getMeetingEndTime().getHourOfDay());
        Assert.assertEquals(0, ((BookingRequest)meetingsRequests[0]).getMeetingEndTime().getMinuteOfHour());
    }


    @Test
    public void testInvalidInputRequest(){
        String requestData = "0900 1730"+"\n"+"2015-08-17 10:17:06 EMP001";
        ScheduledMeetings meetings = scheduler.receiveRequest(requestData);
        Assert.assertNull(meetings);
    }
}
