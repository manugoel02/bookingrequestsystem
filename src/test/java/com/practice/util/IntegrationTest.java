package com.practice.util;

import com.practice.model.ScheduledMeetings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by manugoel on 11/27/2016.
 */
public class IntegrationTest {
    private MeetingScheduler scheduler;

    @Before
    public void setup(){
        scheduler = MeetingScheduler.getInstance();
    }

    @Test
    public void testIntegration(){
        String request = "0900 1730\n" +
                "2015-08-17 10:17:06 EMP001\n" +
                "2015-08-21 09:00 2\n" +
                "2015-08-16 12:34:56 EMP002\n" +
                "2015-08-21 09:00 2\n" +
                "2015-08-16 09:28:23 EMP003\n" +
                "2015-08-22 14:00 2\n" +
                "2015-08-17 11:23:45 EMP004\n" +
                "2015-08-22 16:00 1\n" +
                "2015-08-15 17:29:12 EMP005\n" +
                "2015-08-21 16:00 3";
        ScheduledMeetings meetings = scheduler.receiveRequest(request);
        String expectedOutput = "2015-08-21\n" +
                "09:00 11:00 EMP002\n" +
                "2015-08-22\n" +
                "14:00 16:00 EMP003\n" +
                "16:00 17:00 EMP004\n";
        Assert.assertEquals(expectedOutput, OutputFormatter.outputString(meetings));
    }

    @Test
    public void testGroupMeetingChronologically(){
        String request = "0900 1730\n" +
                "2015-08-16 09:28:23 EMP003\n" +
                "2015-08-22 16:00 1\n" +
                "2015-08-17 11:23:45 EMP004\n" +
                "2015-08-22 14:00 2" ;
        ScheduledMeetings meetings = scheduler.receiveRequest(request);
        String expectedOutput = "2015-08-22\n" +
                "14:00 16:00 EMP004\n" +
                "16:00 17:00 EMP003\n";
        Assert.assertEquals(expectedOutput,OutputFormatter.outputString(meetings));
    }

}
