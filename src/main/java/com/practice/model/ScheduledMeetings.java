package com.practice.model;

/**
 * Created by manugoel on 11/27/2016.
 */
import java.util.Map;
import java.util.Set;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 *
 * @author manugoel
 */
public class ScheduledMeetings {
    private LocalTime officeStartTime;
    private LocalTime officeEndTime;
    private Map<LocalDate,Set<BookingRequest>> bookingRequest;

    public ScheduledMeetings(LocalTime officeStartTime, LocalTime officeEndTime, Map<LocalDate, Set<BookingRequest>> bookingRequest) {
        this.officeStartTime = officeStartTime;
        this.officeEndTime = officeEndTime;
        this.bookingRequest = bookingRequest;
    }


    public Map<LocalDate, Set<BookingRequest>> getBookingRequest() {
        return bookingRequest;
    }

    public void setBookingRequest(Map<LocalDate, Set<BookingRequest>> bookingRequest) {
        this.bookingRequest = bookingRequest;
    }

    public LocalTime getOfficeEndTime() {
        return officeEndTime;
    }

    public void setOfficeEndTime(LocalTime officeEndTime) {
        this.officeEndTime = officeEndTime;
    }

    public LocalTime getOfficeStartTime() {
        return officeStartTime;
    }

    public void setOfficeStartTime(LocalTime officeStartTime) {
        this.officeStartTime = officeStartTime;
    }
}
