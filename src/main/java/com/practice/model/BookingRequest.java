package com.practice.model;

import org.joda.time.Interval;
import org.joda.time.LocalTime;

/**
 *
 * @author manugoel
 */
public class BookingRequest implements Comparable<BookingRequest> {
    private String empId;
    private LocalTime meetingStartTime;
    private LocalTime meetingEndTime;

    public BookingRequest(String empId, LocalTime meetingStartTime, LocalTime meetingEndTime) {
        this.empId = empId;
        this.meetingStartTime = meetingStartTime;
        this.meetingEndTime = meetingEndTime;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public LocalTime getMeetingEndTime() {
        return meetingEndTime;
    }

    public void setMeetingEndTime(LocalTime meetingEndTime) {
        this.meetingEndTime = meetingEndTime;
    }

    public LocalTime getMeetingStartTime() {
        return meetingStartTime;
    }

    public void setMeetingStartTime(LocalTime meetingStartTime) {
        this.meetingStartTime = meetingStartTime;
    }

    public int compareTo(BookingRequest obj) {
        Interval meetingInterval = new Interval(meetingStartTime.toDateTimeToday(),meetingEndTime.toDateTimeToday());
        Interval compareToMeetingInterval = new Interval(obj.meetingStartTime.toDateTimeToday(),
                obj.meetingEndTime.toDateTimeToday());
        if(meetingInterval.overlaps(compareToMeetingInterval)){
            return 0;
        }
        else{
            return meetingStartTime.compareTo(obj.meetingStartTime);
        }
    }


}
