package com.practice.controller;

import com.practice.model.ScheduledMeetings;
import com.practice.util.MeetingScheduler;
import com.practice.util.OutputFormatter;
import javafx.beans.binding.StringBinding;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by manugoel on 11/27/2016.
 */
public class AppMain {
    private static MeetingScheduler meetingScheduler;
    private static Logger logger = Logger.getLogger(AppMain.class);
    public static void main(String args[]){
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please Provide the input batch to be Processed. Press Twice Enter when done with input");
        String receivedRequest = null;
        try {
            while((receivedRequest = br.readLine()) != null && !receivedRequest.trim().equals("") )
            {
                    sb.append(receivedRequest).append("\n");
            }
        } catch (IOException e) {
            System.out.println(e);
            onExit();
        }
        meetingScheduler = MeetingScheduler.getInstance();
        receivedRequest = sb.toString().trim();
        if(receivedRequest.equals("")){
            logger.error("Invalid Request");
            onExit();
        }
        else{
            ScheduledMeetings meetings = meetingScheduler.receiveRequest(receivedRequest);
            System.out.println("***************** Scheduled Meetings **************************");
            System.out.println(OutputFormatter.outputString(meetings));
        }
    }

    public static void onExit(){
        System.exit(-1);
    }
}
