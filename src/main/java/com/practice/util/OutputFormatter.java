package com.practice.util;

/**
 * Created by manugoel on 11/27/2016.
 */
import java.util.Map;
import java.util.Set;

import com.practice.model.*;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author manugoel
 */
public class OutputFormatter {
    private static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    private static  DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("HH:mm");
    private Logger logger = Logger.getLogger(getClass());

    public static String outputString(ScheduledMeetings object){
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<LocalDate,Set<BookingRequest>> meetingEntry : object.getBookingRequest().entrySet()){
            LocalDate meetingDate = meetingEntry.getKey();
            sb.append(dateFormatter.print(meetingDate)).append("\n");
            Set<BookingRequest> requestList = meetingEntry.getValue();
            for(BookingRequest requestObject : requestList){
                sb.append(timeFormatter.print(requestObject.getMeetingStartTime())).append(" ");
                sb.append(timeFormatter.print(requestObject.getMeetingEndTime())).append(" ");
                sb.append(requestObject.getEmpId()).append("\n");
            }
        }
        return sb.toString();
    }

}
