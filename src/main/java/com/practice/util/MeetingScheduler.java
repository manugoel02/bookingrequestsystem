package com.practice.util;

/**
 * Created by manugoel on 11/27/2016.
 */
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.practice.model.*;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 *
 * @author manugoel
 */
public class MeetingScheduler {
    private DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    private DateTimeFormatter separatedTimeFormatter = DateTimeFormat.forPattern("HH:mm");
    private static final MeetingScheduler instance = new MeetingScheduler();
    private Logger logger = Logger.getLogger(getClass());
    private static final int LIMIT = -1;
    private MeetingScheduler(){}
    public static MeetingScheduler getInstance(){
        return instance;
    }

    public ScheduledMeetings receiveRequest(String receivedRequest){
        try{
            String[] requestData = splitData(receivedRequest,"\n");
            if(requestData.length >= 1){
                final String officeTimeTokens[] = splitData(requestData[0],"\\s");
                LocalTime officeStartTime = new LocalTime(Integer.parseInt(officeTimeTokens[0].substring(0,2)),
                        Integer.parseInt(officeTimeTokens[0].substring(2,4)));
                LocalTime officeEndTime = new LocalTime(Integer.parseInt(officeTimeTokens[1].substring(0,2)),
                        Integer.parseInt(officeTimeTokens[1].substring(2,4)));
                Map<LocalDate,Set<BookingRequest>> requestList = new HashMap<LocalDate, Set<BookingRequest>>();
                for(int requestLine= 1; requestLine < requestData.length; requestLine=requestLine+2){
                    String meetingSlot[] = splitData(requestData[requestLine+1],"\\s");
                    LocalDate meetingDate = dateFormatter.parseLocalDate(meetingSlot[0]);
                    BookingRequest meetingReqObject = parseRecord(requestData[requestLine], officeStartTime, officeEndTime, meetingSlot);
                    if( null != meetingReqObject){
                        if(requestList.containsKey(meetingDate)){
                            requestList.get(meetingDate).remove(meetingReqObject);
                            requestList.get(meetingDate).add(meetingReqObject);
                        }
                        else{
                            Set<BookingRequest> meetingForDay = new TreeSet<BookingRequest>();
                            meetingForDay.add(meetingReqObject);
                            requestList.put(meetingDate, meetingForDay);
                        }
                    }
                }
                return new ScheduledMeetings(officeStartTime,officeEndTime,requestList);
            }
            else
            {
                logger.error("Invalid Request");
                return null;
            }
        }
        catch(Exception e){
            logger.error("Exception caught "+ e);
            return null;
        }
    }

    public String[] splitData(String data,String regex){
        return data.split(regex,LIMIT);
    }

    public BookingRequest parseRecord(String record,LocalTime officeStartTime,LocalTime officeEndTime,String[] meetingSlots){
        String requestTokens[] = splitData(record,"\\s+");
        String empId = requestTokens[2];
        LocalTime meetingStartTime = separatedTimeFormatter.parseLocalTime(meetingSlots[1]);
        LocalTime meetingEndTime = new LocalTime(meetingStartTime.getHourOfDay(),meetingStartTime.getMinuteOfHour())
                .plusHours(Integer.parseInt(meetingSlots[2]));
        if(invalidMeeting(officeStartTime, officeEndTime, meetingStartTime, meetingEndTime)){
            logger.error("Employeed ID "+empId+" has booked meeting outside office hours");
            return null;
        }
        else{
            return new BookingRequest(empId, meetingStartTime, meetingEndTime);
        }
    }

    public boolean invalidMeeting(LocalTime officeStartTime,LocalTime officeFinishTime,
                                  LocalTime meetingStartTime,LocalTime meetingFinishTime) {
        return meetingStartTime.isBefore(officeStartTime) || meetingStartTime.isAfter(officeFinishTime) ||
                meetingFinishTime.isAfter(officeFinishTime) || meetingFinishTime.isBefore(officeStartTime);
    }
}